const { parse } = require("path");
const { domainToUnicode } = require("url");

function calcular() {
    let txtPrecio = document.getElementById("txtPrecio");
    let txtSubTotal = document.getElementById("txtSubTotal");
    let txtImpuesto = document.getElementById("txtImpuesto");
    let txtEdad = document.getElementById("txtEdad");
    let txtTotalPagar = document.getElementById("txtTotalPagar");
    let optTipo = document.getElementById("optTipo");
    let txtDescuento = document.getElementById("txtDescuento");

    if(optTipo.value == 1)
        txtSubTotal.value = txtPrecio.value;
    else
        txtSubTotal.value = parseFloat(txtPrecio.value*.80) + parseFloat(txtPrecio.value); 

    txtImpuesto.value = parseFloat(txtSubTotal.value *.16);

    if(txtEdad.value >= 60)
        txtDescuento.value = parseFloat(txtPrecio.value*.50);
    else
        txtDescuento.value = 0;

    txtTotalPagar.value = (parseFloat(txtSubTotal.value) + parseFloat(txtImpuesto.value)) - parseFloat(txtDescuento.value);
}

function limpiar() {
    let txtNumBoleto = document.getElementById("txtNumBoleto");
    let txtDestino = document.getElementById("txtDestino");
    let txtNombreCliente = document.getElementById("txtNombreCliente");
    let txtEdad = document.getElementById("txtEdad");
    let txtPrecio = document.getElementById("txtPrecio");
    let txtSubTotal = document.getElementById("txtSubTotal");
    let txtImpuesto = document.getElementById("txtImpuesto");
    let txtDescuesto = document.getElementById("txtDescuento");
    let txtTotalPagar = document.getElementById("txtTotalPagar");

    txtNumBoleto.value = " ";
    txtDestino.value = " ";
    txtNombreCliente.value = " ";
    txtEdad.value = " ";
    txtPrecio.value = " ";
    txtSubTotal.value = " ";
    txtImpuesto.value = " ";
    txtDescuesto.value = " ";
    txtTotalPagar.value = " ";
}