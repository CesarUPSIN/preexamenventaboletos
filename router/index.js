const express = require("express");
const router = express.Router();
const bodyParser = require("body-parser");

router.get("/ventaBoletos", (req, res)=> {
    const params = {
        txtNumBoleto: req.query.txtNumBoleto,
        txtDestino: req.query.txtDestino,
        txtNombreCliente: req.query.txtNombreCliente,
        txtEdad: req.query.txtEdad,
        optTipo: req.query.optTipo,
        txtPrecio: req.query.txtPrecio,
    }
    res.render("ventaBoletos.html", params);
});

router.post("/ventaBoletos", (req, res)=> {
    const params = {
        txtNumBoleto: req.body.txtNumBoleto,
        txtDestino: req.body.txtDestino,
        txtNombreCliente: req.body.txtNombreCliente,
        txtEdad: req.body.txtEdad,
        optTipo: req.body.optTipo,
        txtPrecio: req.body.txtPrecio,
    }
    res.render("ventaBoletos.html", params);
});

module.exports = router;