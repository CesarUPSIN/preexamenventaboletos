const http = require("http");
const express = require("express");
const bodyparser = require("body-parser");

const misRutas = require("./router/index.js");
const path = require("path");
const app = express();

app.set("view engine", "ejs");
app.use(express.static(__dirname + '/public'));
app.use(bodyparser.urlencoded({extended:true}));

app.engine("html", require("ejs").renderFile);
app.use(misRutas);

const puerto = 5000;

app.listen(puerto, ()=> {
    console.log("Iniciando puerto");
});